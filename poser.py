#!/usr/bin/python
# poser.py
# a python script to get the distance, azimuth and elevation from adb device
# Luis M. Sanchez B. 4/23/2017. UCSD MAS-WES Capstone

import subprocess, sys
import re
from decimal import *
import logging
import time
from math import *

logging.basicConfig(level=logging.INFO)
ADBDEV=True

def action_function():
   x = 0.0
   y = 0.0
   z = 0.0
   dist = elev = azim = 0.0

   ###### root and remount adb device ######

   if ADBDEV:
      cmd = ['adb','start-server']
      subprocess.call(cmd)
      cmd = ['adb', 'root']
      ret = subprocess.check_output(cmd)
      logging.info(ret)
      match = re.search("^adbd", ret, re.MULTILINE)
      if(match):
         logging.info("root ok")
      else:
         logging.info("failed to root")
         sys.exit()

      cmd = ['adb', 'wait-for-devices']
      subprocess.call(cmd)
      cmd = ['adb', 'remount']
      ret = subprocess.check_output(cmd)
      logging.info(ret)
      match = re.search("^remount succeeded", ret, re.MULTILINE)
      if(match):
         logging.info("Remounted device :)")
      else:
         logging.info("Failed to remount :(")
         sys.exit(1)

   ###### Excute scripts  ######

   if ADBDEV:
      cmd = ['adb', 'shell', '/data/misc/wifi/ftm.sh']
   else:
      cmd = ['sh', 'dist.sh']
   ret = subprocess.check_output(cmd)
   logging.info(ret)
   match = re.search("^Distance\(cm\):\s(-*\d+.\d+)",ret, re.MULTILINE)
   if(match):
      dist = float(match.group(1))
   else:
      logging.error("No distance :(")

   if ADBDEV:
      cmd = ['adb', 'shell', '/data/misc/wifi/aoa.sh']
   else:
      cmd = ['sh', 'azim.sh']
   ret = subprocess.check_output(cmd)
   logging.info(ret)
   match = re.search("^Azimuth:\s(-*\d+.\d+)\sElevation:\s(-*\d+.\d+)",ret,re.MULTILINE)
   if(match):
      azim = float(match.group(1))*pi/180
      elev = float(match.group(2))*pi/180
   else:
      logging.error("No azimuth nor elevation  :(")

   x = dist * sin(elev) * cos(azim)
   y = dist * sin(elev) * sin(azim)
   z = dist * cos(elev)

   print "x = ", x
   print "y = ", y
   print "z = ", z


if __name__ == "__main__":
   try:
      action_function()
   except:
      print "python poser.py failed"
      raise
      sys.exit()

