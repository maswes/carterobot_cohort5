#!/usr/bin/env python
# wigig_location_monitor.py
#
# Turtlebot follows 11AD AP with obstraicles avoidance
# TurtleBot must have minimal.launch & amcl_demo.launch running prior to starting this script.
#
# Yun Park 5/11/2017. UCSD MAS-WES Capstone

import rospy
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
from nav_msgs.msg import Odometry

class WigigFollowerAvoid():
    def __init__(self):
        rospy.init_node('wigig_follower_avoid', anonymous=False)

	#what to do if shut down (e.g. ctrl + C or failure)
	rospy.on_shutdown(self.shutdown)

        self.wait = 2 #wait in sec before move to next target
	
	#tell the action client that we want to spin a thread by default
	self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
	rospy.loginfo("wait for the action server to come up")
	#allow up to 5 seconds for the action server to come up
	self.move_base.wait_for_server(rospy.Duration(5))

	rospy.Subscriber("/wigig", Odometry, self.wigig_cb)
	rospy.loginfo("Following Wigig AP...")
 

    def wigig_cb(self, msg): 
	#we'll send a goal to the robot to the Wigig AP
	goal = MoveBaseGoal()
	goal.target_pose.header.frame_id = 'base_link'
	goal.target_pose.header.stamp = rospy.Time.now()
	goal.target_pose.pose.position.x = -msg.pose.pose.position.x * 0.01 #cm -> m
	goal.target_pose.pose.position.y = -msg.pose.pose.position.y * 0.01 
	goal.target_pose.pose.orientation.w = 1.0 #go forward

	#start moving
        rospy.loginfo("Move to (%f,%f)", goal.target_pose.pose.position.x, goal.target_pose.pose.position.y) 
        self.move_base.send_goal(goal)

        #allow TurtleBot move toward target
        success = self.move_base.wait_for_result(rospy.Duration(self.wait))

        if success:
            # We made it!
            state = self.move_base.get_state()
            if state == GoalStatus.SUCCEEDED:
                rospy.loginfo("Hooray, the base reached target")



    def shutdown(self):
        rospy.loginfo("Stop")

if __name__ == '__main__':
    try:
        WigigFollowerAvoid()
        # spin() simply keeps python from exiting until this node is stopped
        rospy.spin()
	
    except rospy.ROSInterruptException:
        rospy.loginfo("Exception thrown")

