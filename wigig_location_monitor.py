#!/usr/bin/python
# wigig_location_monitor.py
#
# Receive 11AD location data from MTP and publish
# nav_msgs/Odometry messages to the 'wigig' topic
#
# Yun Park 4/29/2017. UCSD MAS-WES Capstone
import numpy as np
import rospy
import roslib
import subprocess, sys
import re
from decimal import *
import logging
import time
from math import *
import threading

# messages
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Quaternion
from geometry_msgs.msg import Twist

logging.basicConfig(level=logging.INFO)
MTPDEV=False
ADBDEV=False

dist_vel = 1      #5cm/s
angle_vel = 0.1  #15 deg/s

def init_adb():
   ###### root and remount adb device ###### 
   if ADBDEV: 
      cmd = ['adb','start-server'] 
      subprocess.call(cmd) 
      cmd = ['adb', 'root'] 
      ret = subprocess.check_output(cmd) 
      logging.info(ret) 
      match = re.search("^adbd", ret, re.MULTILINE) 
      if(match): 
         logging.info("root ok") 
      else: 
         logging.info("failed to root") 
         sys.exit() 
 
      cmd = ['adb', 'wait-for-devices'] 
      subprocess.call(cmd) 
      cmd = ['adb', 'remount'] 
      ret = subprocess.check_output(cmd) 
      logging.info(ret) 
      match = re.search("^remount succeeded", ret, re.MULTILINE) 
      if(match): 
         logging.info("Remounted device :)") 
      else: 
         logging.info("Failed to remount :(") 
         sys.exit(1) 


class wigig_location:
    def __init__(self):
        if not MTPDEV: 
            self.lock = threading.Lock()
            self.dist = 0.0
            self.azim = 0.0
            self.elev = 0.0


    def get_wigig_location(self):
       x = 0.0
       y = 0.0
       z = 0.0

       if MTPDEV: 
          ###### Excute scripts  ######
          dist = elev = azim = 0.0

          if ADBDEV:
             cmd = ['adb', 'shell', '/data/misc/wifi/dist.sh']
          else:
             cmd = ['sh', 'dist.sh']
          ret = subprocess.check_output(cmd)
          logging.info(ret)
          match = re.search("^Distance\(cm\):\s(-*\d+.\d+)",ret, re.MULTILINE)
          if(match):
             dist = float(match.group(1))
          else:
             logging.error("No distance :(")

          if ADBDEV:
             cmd = ['adb', 'shell', '/data/misc/wifi/azim.sh']
          else:
             cmd = ['sh', 'azim.sh']
          ret = subprocess.check_output(cmd)
          logging.info(ret)
          match = re.search("^Azimuth:\s(-*\d+.\d+)\sElevation:\s(-*\d+.\d+)",ret,re.MULTILINE)
          if(match):
             azim = float(match.group(1))*pi/180.0 #radians
             elev = float(match.group(2))*pi/180.0 #radians
          else:
             logging.error("No azimuth nor elevation  :(")
       else:
          # For test, set fixed value
          with self.lock:
              self.dist = self.dist + dist_vel
              self.azim = self.azim + angle_vel
              self.elev = self.elev + angle_vel
              dist = self.dist
              azim = self.azim
              elev = self.elev

       dist = dist * 0.01 #m
       #x = dist * sin(elev) * cos(azim) #m
       #y = dist * sin(elev) * sin(azim) #m
       #z = dist * cos(elev)
       x = dist * cos(elev) * sin(azim) #m
       y = dist * cos(elev) * cos(azim) #m
       z = dist * sin(elev)
       d = dist #m
       a = azim #radians

       return x, y, z, d, a


# Covariance
P = np.mat(np.diag([0.0]*3))
cov_x = 0.1
cov_y = 0.1
cov_z = 0.1

def wigig_location_monitor(wigig):
    pub = rospy.Publisher('wigig', Odometry, queue_size=10)
    rate = rospy.Rate(5) #monitoring rate in Hz

    while not rospy.is_shutdown():
	x, y, z, d, a = wigig.get_wigig_location()
        rospy.loginfo(rospy.get_caller_id() + ' (%f,%f,%f,%f,%f)', x, y, z, d, a)

        if MTPDEV and (x == 0 or y == 0 or z == 0):
            continue

        # publish only valid data
        wigig_data = Odometry()
        wigig_data.header.stamp = rospy.Time.now()
        #wigig_data.header.frame_id = '/base_footprint'
        wigig_data.header.frame_id = '/odom_combined'
        wigig_data.child_frame_id = '/base_footprint'
        wigig_data.pose.pose.position = Point(x, y, z)
        wigig_data.pose.pose.orientation.x = 1         # identity quaternion
        wigig_data.pose.pose.orientation.y = 0         # identity quaternion
        wigig_data.pose.pose.orientation.z = 0         # identity quaternion
        wigig_data.pose.pose.orientation.w = 0         # identity quaternion

        wigig_data.twist.twist.linear.x = d     # distance in m from the AP
        wigig_data.twist.twist.angular.z = a    # angle in radian from the AP

        p_cov = np.array([0.0]*36).reshape(6,6)
        # position covariance
        p_cov[0,0] = cov_x
        p_cov[1,1] = cov_y
        p_cov[2,2] = cov_z
        # orientation covariance
        p_cov[3,3] = 99999
        p_cov[4,4] = 99999
        p_cov[5,5] = 99999

        wigig_data.pose.covariance = tuple(p_cov.ravel().tolist())

        pub.publish(wigig_data)

        rate.sleep()

if __name__ == '__main__':
    try:
        init_adb()
        rospy.init_node('wigig_location_monitor', anonymous=True)
        wigig = wigig_location()
        wigig_location_monitor(wigig)

    except rospy.ROSInterruptException:
        pass
