#!/usr/bin/env python
# wigig_location_monitor.py
#
# Turtlebot follows 11AD AP without obstraicles avoidance
#
# Yun Park 5/11/2017. UCSD MAS-WES Capstone

import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from math import *
import logging

logging.basicConfig(level=logging.INFO)

FIXED_AP=False
SUB_RATE=10   #subscription rate in Hz
MOVE_RATE=5   #Moving rate 5Hz = 0.2 sec duration
DIST_VEL=0.15 #m/s
ANGLE_VEL=radians(45) #deg/s 
STOP_DIST=0.5 #Turtlebot will stop at 0.5 m
CLOSEST_DIST=0.3 #Turtlebot will move backward at < 0.3 m
CLOSEST_ANGLE=radians(10) #Turtlebot will start to move at > 10 degrees
MAX_DIST_VEL=1 #1 m/s
MAX_ANGLE_VEL=radians(180) #180 deg/s 
VARIABLE_VEL=True #variable velocity

class WigigFollower():
    def __init__(self):
        rospy.init_node('wigig_follower', anonymous=False)

	#what to do if shut down (e.g. ctrl + C or failure)
	rospy.on_shutdown(self.shutdown)

        self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)

	rospy.Subscriber("/wigig", Odometry, self.wigig_cb)
	
        self.sub_rate = rospy.Rate(SUB_RATE)
        self.move_rate = rospy.Rate(MOVE_RATE)

	# create two different Twist() variables.  One for moving forward.  One for turning 45 degrees.

        #go forward at 0.2 m/s
        self.dist_vel = DIST_VEL
        #turn at 45 deg/s
        self.angle_vel = ANGLE_VEL  #radians/s

        #move forward command
        self.move_fwd_cmd = Twist()
        self.move_fwd_cmd.linear.x = self.dist_vel
        #move backward command
        self.move_bwd_cmd = Twist()
        self.move_bwd_cmd.linear.x = -self.dist_vel
	# by default angular.z is 0 so setting this isn't required

        #trun right command
        self.turn_right_cmd = Twist()
        self.turn_right_cmd.linear.x = 0
        self.turn_right_cmd.angular.z = self.angle_vel
        #trun left command
        self.turn_left_cmd = Twist()
        self.turn_left_cmd.linear.x = 0
        self.turn_left_cmd.angular.z = -self.angle_vel

	# create one Twist() variables for both moving forward and turning 45 degrees.
        self.move_right_cmd = Twist()
        self.move_right_cmd.linear.x = self.dist_vel
        self.move_right_cmd.angular.z = self.angle_vel
        self.move_left_cmd = Twist()
        self.move_left_cmd.linear.x = self.dist_vel
        self.move_left_cmd.angular.z = -self.angle_vel

	rospy.loginfo("Following Wigig AP...")
 

    def wigig_cb(self, msg): 
	#we'll send a goal to the robot to the Wigig AP
	dist = msg.twist.twist.linear.x    # distance in meter from the AP
	angle = msg.twist.twist.angular.z  # angle in radian from the AP

        rospy.loginfo("Move to (dist=%f m (%f m/s), angle=%f d (%f r/s))", dist, self.dist_vel, degrees(angle), self.angle_vel) 

        if VARIABLE_VEL:
            self.dist_vel = DIST_VEL * dist
            self.angle_vel = ANGLE_VEL * abs(angle)

            if self.dist_vel > MAX_DIST_VEL:	
                self.dist_vel = MAX_DIST_VEL	
            if self.angle_vel > MAX_ANGLE_VEL:	
                self.angle_vel = MAX_ANGLE_VEL	

            #set valocity
            #move forward command
            self.move_fwd_cmd.linear.x = self.dist_vel
            #move backward command
            #self.move_bwd_cmd.linear.x = -self.dist_vel
            #trun right command
            self.turn_right_cmd.angular.z = self.angle_vel
            #trun left command
            self.turn_left_cmd.angular.z = -self.angle_vel
	    #moving forward and turning 45 degrees.
            self.move_right_cmd.linear.x = self.dist_vel
            self.move_right_cmd.angular.z = self.angle_vel
            self.move_left_cmd.linear.x = self.dist_vel
            self.move_left_cmd.angular.z = -self.angle_vel

	#Go forward for dist/dist_vel seconds then turn for angle/angle_vel second
        if FIXED_AP:
	    dist_count = trunc(MOVE_RATE * dist/self.dist_vel)
	    angle_count = trunc(MOVE_RATE * angle/self.angle_vel)

	    # turn angle degrees
	    rospy.loginfo("Turning %f degree", degrees(angle))
            if angle != 0.0:
                for x in range(0, angle_count):
                    if angle > 0.0:
                        self.cmd_vel.publish(self.turn_right_cmd)
                    else:
                        self.cmd_vel.publish(self.turn_left_cmd)
                    self.move_rate.sleep()            
	    # go forward dist m
	    rospy.loginfo("Going Straight %f m", dist)
            if dist != 0.0:
                for x in range(0, dist_count):
                    if dist > 0.0: 
                        self.cmd_vel.publish(self.move_fwd_cmd)
                    else:
                        self.cmd_vel.publish(self.move_bwd_cmd)
                    self.move_rate.sleep()
        else:
            if abs(angle) > CLOSEST_ANGLE and dist > STOP_DIST:
                if angle < CLOSEST_ANGLE:
	            rospy.loginfo("Move right forwad")
                    self.cmd_vel.publish(self.move_right_cmd)
                else:
	            rospy.loginfo("Move left forwad")
                    self.cmd_vel.publish(self.move_left_cmd)
                self.sub_rate.sleep()

            elif abs(angle) > CLOSEST_ANGLE:
                if angle < CLOSEST_ANGLE:
	            rospy.loginfo("Turn right")
                    self.cmd_vel.publish(self.turn_right_cmd)
                else:
	            rospy.loginfo("Turn left")
                    self.cmd_vel.publish(self.turn_left_cmd)
                self.sub_rate.sleep()

            elif dist > STOP_DIST:
	        rospy.loginfo("Move forward")
                self.cmd_vel.publish(self.move_fwd_cmd)
                self.sub_rate.sleep()

            elif dist < CLOSEST_DIST:
	        rospy.loginfo("Move backward")
                self.cmd_vel.publish(self.move_bwd_cmd)
                self.sub_rate.sleep()

    def shutdown(self):
        rospy.loginfo("Stop")

if __name__ == '__main__':
    try:
        WigigFollower()
        # spin() simply keeps python from exiting until this node is stopped
        rospy.spin()
	
    except rospy.ROSInterruptException:
        rospy.loginfo("Exception thrown")

